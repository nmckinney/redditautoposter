#ifndef REDDIT_H
#define REDDIT_H

#include <QObject>
#include <QString>
#include <QSettings>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonArray>
#include <QHttpMultiPart>
#include <QHttpPart>
#include <QList>
#include <QFile>
#include <QTextStream>


enum HTTP_REQUEST_TYPE
{
    GET = 1,
    POST
};

enum REDDIT_VOTE_TYPE
{
    UPVOTE = 1,
    DOWNVOTE = -1,
};

class Reddit : public QObject
{
    Q_OBJECT
private:
    QNetworkAccessManager* qnam;
    QUrl articleURL;
    QString commentText;
    QString modhash;
    QString thingID;
    REDDIT_VOTE_TYPE vote;

    QString username, password;

    void login(void);
    void sendRequest(QUrl url, HTTP_REQUEST_TYPE type_request = GET, QList<QHttpPart>* list = 0);
    void upvote(void);
    void downvote(void);
public:
    explicit Reddit(QObject *parent = 0, QString articleURL = "", QString account = "", QString password = "", REDDIT_VOTE_TYPE vote = UPVOTE);
signals:
    void finished(void);
    void error(void);
    void loggedIn(void);
    void gotArticle(void);
    void voted(void);
public slots:
    void start(void);
    void gotReply(QNetworkReply* reply);
private slots:
    void sendCode(void);
    void getArticle(void);
};

#endif // REDDIT_H
