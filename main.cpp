#include <QCoreApplication>
#include "reddit.h"
#include <QPair>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QtXml>
#include <QVector>
#include <iostream>
#include <QDir>

typedef QPair<QString, QString> Account;
typedef QVector<Account> AccountList;

AccountList parseConfig(QDir path)
{
    AccountList accounts;
    QFile file(path.absolutePath() + "/config.xml");
    std::cout << "[*] Parsing configuration file...\n";
    if (!file.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        std::cout << "[!] Error: No configuration file found.\n";
        exit(1);
    }
    QDomDocument xmlDoc;
    xmlDoc.setContent(file.readAll());
//    qDebug() << xmlDoc.toString();
    QDomElement element = xmlDoc.documentElement();
//    qDebug() << "Root name:" << element.nodeName();
    std::cout << "[*] Opened file...\n";
    if (!element.hasChildNodes())
    {
        std::cout << "[!] Error: No accounts in configuration file. Quitting...\n";
        exit(1);
    }
    QDomNodeList list = element.elementsByTagName("Account");
    int i;
    for(i = 0; i < list.count(); i++)
    {
        QString username = list.at(i).firstChild().toElement().text();
        QString password = list.at(i).lastChild().toElement().text();
        Account acc;
        acc.first = username;
        acc.second = password;
        accounts.append(acc);
    }
    std::cout << "[*] " <<  i << " accounts added.\n";
    return accounts;
}

void help(void)
{
    std::cout << "RedditAutoPoster: Automate massive amounts of upvotes or downvotes. Reddit accounts not included!\n";
    std::cout << "------------------\n";
    std::cout << "Written by Nicholas McKinney!\n\n";
    std::cout << "Options:\n";
    std::cout << "-h | --help\n";
    std::cout << "\t Prints help screen. You're looking at it!\n";
    std::cout << "-u | --upvote\n";
    std::cout << "\t Sets program to upvote the article given.\n";
    std::cout << "-d | --downvote\n";
    std::cout << "\t Sets program to downvote the article given.\n";
    std::cout << "I don't know if I need to say this, but you can only choose to either upvote or downvote!\n";
}

void usage(void)
{
    std::cout << "Usage: RedditAutoPoster [-u | -d | --upvote | --downvote] http://reddit.com/r/{subreddit}/{article}\n";
    std::cout << "\n\n";
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    REDDIT_VOTE_TYPE ud;
    QString articleUrl;
    if(argc < 3)
    {
        usage();
        for (int i = 0; i < argc; i++)
        {
            if (argv[i] == "-h" || argv[i] == "--help") help();
        }
        exit(1);
    }
    if (argv[1] != "-u" || argv[1] != "--upvote" || argv[1] != "-d" || argv[1] != "--downvote")
    {
        usage();
        exit(1);
    }
    if (argv[1] == "-u" || argv[1] == "--upvote") ud = UPVOTE;
    if (argv[1] == "-d" || argv[1] == "--downvote") ud = DOWNVOTE;
    articleUrl = argv[2];
    std::cout << "Reddit Auto-Voter\n";
    std::cout << "---------------------\n";

    AccountList accs = parseConfig(a.applicationDirPath());
    for(int i = 0; i < accs.count(); i++)
    {
        Reddit redd(0, articleUrl, ((Account)(accs.at(i))).first, ((Account)(accs.at(i))).second, ud);
        redd.start();
    }
    return a.exec();
}
