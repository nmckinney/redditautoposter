#-------------------------------------------------
#
# Project created by QtCreator 2013-02-14T11:47:33
#
#-------------------------------------------------

QT       += core network xml

QT       -= gui

TARGET = RedditAutoPoster
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    reddit.cpp

HEADERS += \
    reddit.h
